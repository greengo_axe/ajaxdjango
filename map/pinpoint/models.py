from django.db import models


class Marker(models.Model):
    name = models.CharField(max_length=250)
    lat = models.DecimalField(max_digits=8, decimal_places=6)
    lng = models.DecimalField(max_digits=9, decimal_places=6)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.BooleanField(default=1)

    class Meta:
        ordering = ('-name',)

    def __str__(self):
        return self.name
