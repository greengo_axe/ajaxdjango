from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_GET
from .models import Marker
from .decorator import ajax_only


def marker_homepage(request):
    return render(request,
                  'pinpoint/marker/list.html',
                  )


@ajax_only
@require_GET
def marker_ajax(request):
    markers = (Marker.objects.filter(status=1)
               .values('name', 'lat', 'lng'))

    return JsonResponse({'markers': list(markers)})
