from django.contrib import admin
from .models import Marker


@admin.register(Marker)
class MarkerAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'lat', 'lng')
    list_filter = ('status', 'name')
    ordering = ('name', 'status')
