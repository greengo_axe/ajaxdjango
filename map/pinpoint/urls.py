from django.urls import path
from . import views

app_name = 'pinpoint'

urlpatterns = [
    path('', views.marker_homepage, name='pinpoint_homepage'),
    path('markers/', views.marker_ajax, name='marker_ajax')
]
