# Django, jQuery, JSON and AJAX

## Abstract 
Data showed on a map through Django, jQuery, JSON and AJAX

For the PHP version of this project, click [here](https://gitlab.com/greengo_axe/demomap) 


## Premise
This project requires :

* Python 3.8

It has to be up and running. 

## Installation
* Python Virtual Environment (venv) from a terminal (bash shell):

>> **mkdir /var/www/html/ajaxdjango**

>> **chmod -R 777 /var/www/html/ajaxdjango**

>> **cd /var/www/html/ajaxdjango**

>> **python -m venv venv**

>> **source venv/bin/activate**

>> **pip install "Django==3.1.5"**

>> *  In the venv, download and install:
>>> * asgiref              3.3.1
>>> * django-debug-toolbar 3.2
>>> * django-taggit        1.2.0
>>> * psycopg2-binary      2.8.6
>>> * pytz                 2020.4
>>> * setuptools           41.6.0
>>> * sqlparse             0.4.1

>> **django-admin startproject map**

>> **cd map**

>> **python manage.py migrate**

>> **python manage.py createsuperuser** 

>>> and follow the procedure on the screen


* The root folder of the project is 
 
    >**ajaxdjango** 
    
* and the absolute path is
 
    >**/var/www/html/ajaxdjango**
    
* Clone the project from gitlab on your Desktop :
    
    >**git@gitlab.com:greengo_axe/ajaxdjango.git**

* copy and overwrite all folders and files from 

>>**Desktop/ajaxdjango**

>into 

>>**/var/www/html/ajaxdjango**

* Delete the folder on your Desktop (**Desktop/ajaxdjango**)

* Start the Django's built-in server: 

> **/var/www/html/ajaxdjango/map> $ python manage.py runserver**

* Open your favourite browser and type this URL:

> **localhost:8000**

* Enjoy!


## F.A.Q

**To insert new entries into the db**

> * Open your favourite browser and type this URL: **localhost:8000/admin**

> * fill in the form with your credentials set in the step "create a super user"

> * click on the "marker" link 

> * click on the "add" link 

> * fill in the form

> * click on the "save" button

> * Enjoy!


## Troubleshooting
  * This project has been developed on Linux OS and there might be a few problems with the path or to grant the right permissions on Windows / Apple OSs.
  
 
## TODO List:
 * To improve CSS style
 * Whatever you like
